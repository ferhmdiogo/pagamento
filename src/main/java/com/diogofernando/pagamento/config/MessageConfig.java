package com.diogofernando.pagamento.config;

import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
public class MessageConfig {

	
	@Value("${crud.rabbitmq.exchange}")
	String exchange;
	
	
	public Exchange declareExchage() {
		return ExchangeBuilder.directExchange(exchange).durable(true).build();
	}
	

	public MessageConverter jsonMessageConverter() {
		return new Jackson2JsonMessageConverter();
	}
}